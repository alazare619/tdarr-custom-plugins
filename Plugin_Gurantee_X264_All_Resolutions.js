function details() {

    return {
      id: "7xcQa",
      Name: "alazare619 HandBrake Fast convert all non x264, keeping original container format",
      Type: "Video",
      Description: `[Contains built-in filter] This plugin transcodes into H264 using HandBrake's 'Fast XYZp30' preset if the file is not in x264 already.\n`,
      Version: "1.00",
      Link: "https://google.com"
    }
  }
function plugin(file) {

if(file.ffProbeData.streams[0].codec_name.toLowerCase() != 'h264'){
    response.infoLog += '"☒File is not in h264 \n'
    switch(file.ffProbeData.streams[0].video_resolution.toLowerCase()){
        case "1080p":
            response.infoLog += "☒File is 1080p \n";
            response.preset = '-Z "Fast 1080p30"';
            response.reQueueAfter = true;
            response.processFile = true;
            response.handBrakeMode = true;
            response.container = '.' + file.container;
            NeedsProcessed = true;
            return response;
            break;
        case "720p":
            response.infoLog += "☒File is 720p \n";
            response.preset = '-Z "Fast 720p30"';
            response.reQueueAfter = true;
            response.processFile = true;
            response.handBrakeMode = true;
            response.container = '.' + file.container;
            NeedsProcessed = true;
            return response;
            break;
        case "576p":
            response.infoLog += "☒File is 576p \n";
            response.preset = '-Z "Fast 576p30"';
            response.reQueueAfter = true;
            response.processFile = true;
            response.handBrakeMode = true;
            response.container = '.' + file.container;
            NeedsProcessed = true;
            return response;
            break;
        case "480p":
            response.infoLog += "☒File is 480p \n";
            response.preset = '-Z "Fast 480p30"';
            response.reQueueAfter = true;
            response.processFile = true;
            response.handBrakeMode = true;
            response.container = '.' + file.container;
            NeedsProcessed = true;
            return response
            break;
        default:
            response.infoLog += "☒File is ${file.ffProbeData.streams[0].video_resolution.toLowerCase()}, No handler defined for this defaulting to 720p \n";
            response.preset = '-Z "Fast 720p30"';
            response.reQueueAfter = true;
            response.processFile = true;
            response.handBrakeMode = true;
            response.container = '.' + file.container;
            NeedsProcessed = true;
            return response
            break;
    }
    }else{
        response.infoLog += "☑File is already x264. Skipping conversion.\n"
    }
}
