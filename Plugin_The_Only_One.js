function details() {

  return {
    id: "Tdarr_Plugin_az619_alazare619_HandBrake_H264_Fast720p30",
    Name: "alazare619 HandBrake Fast720p30, No title meta, no subs, 192Kb AAC stereo,Same as Original ",
    Type: "Video",
    Description: `[Contains built-in filter] This plugin transcodes into H264 using HandBrake's 'Fast 720p30' preset if the file is not in H264 already. It removes subs, metadata (if a title exists) and adds a stereo 192kbit AAC track if an AAC track (any) doesn't exist. The output container is MP4. \n\n
`,
    Version: "1.00",
    Link: "https://github.com/alazare619/Tdarr_Plugins/blob/master/Community/Tdarr_Plugin_b9hc_alazare619_HandBrake_H264_Fast720p30.js"
  }

}

function plugin(file) {


/// Must return this object

  var response = {

     processFile : false,
     preset : '',
     container : '.mp4',
     handBrakeMode : false,
     FFmpegMode : false,
     reQueueAfter : false,
     infoLog : '',

  }
/// Abort if not Video
    if (file.fileMedium !== "video") {
      console.log("File is not video")
      response.infoLog += "☒File is not video \n"
      response.processFile = false;

      return response

    }else{
    response.infoLog += "☑File is a video and meets conditions \n"
    }
/// Make sure video is first stream
    if(file.ffProbeData.streams[0].codec_type != 'video') {

      response.infoLog += "Video is not in the first stream"
      response.preset = ',-map 0:v? -map 0:a? -map 0:s? -map 0:d? -map 0:t? -c copy'
      response.reQueueAfter = true;
      response.processFile = true;
      return response

      }else{

      response.infoLog += "File has video in first stream meeting conditions\n"
      }
/// Set Global Variable section
    var jsonString = JSON.stringify(file)
    var NeedsProcessed = false

/////////////////////////////////////////////////// BEGIN VIDEO SECTIONS
/// Function for Worker Gurantee "X264"
    if(file.ffProbeData.streams[0].codec_name.toLowerCase() != 'h264' && file.ffProbeData.streams[0].video_resolution.toLowerCase() == "720p"){

      response.infoLog += "☒File is not in h264 but is 720p \n"
      response.preset = '-Z "Fast 720p30"'
      response.reQueueAfter = true;
      response.processFile = true;
      response.handBrakeMode = true;
      response.container = '.' + file.container
      NeedsProcessed = true;
      return response

     }else{
      response.infoLog += "☑File is already in h264 and 720p! \n"
     }
////////////////////////////////////
if(file.ffProbeData.streams[0].codec_name.toLowerCase() != 'h264' && file.ffProbeData.streams[0].video_resolution.toLowerCase() == "1080p"){

  response.infoLog += "☒File is not in h264 but is 1080p \n"
  response.preset = '-Z "Fast 1080p30"'
  response.reQueueAfter = true;
  response.processFile = true;
  response.handBrakeMode = true;
  response.container = '.' + file.container
  NeedsProcessed = true;
  return response

 }else{
  response.infoLog += "☑File is already in h264 and 720p! \n"
 }
///////////////////////////////////////////////////////////////////////////////////////////////////////////// BEGIN DATA SECTIONS
for (var i = 0; i < file.ffProbeData.streams.length; i++) {
    var DataFilesExist = false
       try {
        if(file.ffProbeData.streams[i].codec_type.toLowerCase() == "subtitle" || file.ffProbeData.streams[i].codec_type.toLowerCase() == "attachment" || file.ffProbeData.streams[i].codec_type.toLowerCase() == "data"){

           DataFilesExist = true

         }
        catch (err) { }
    }
}
  if(DataFilesExist){

    response.infoLog += "☒File has subs, attachments, or binary data \n"
    response.preset = ',-map 0:v? -map 0:a? -c copy -map_metadata -1'
    response.reQueueAfter = true;
    response.processFile = true;
    response.handBrakeMode = false;
    response.FFmpegMode = true;
    response.container = '.' + file.container
    NeedsProcessed = true;
    return response

     }else{
     response.infoLog += "☑File has no subs, attachments, or binary data \n"
     }
/// MetaData in Title
  if(file.meta.Title != undefined ){

    response.infoLog += "☒File has title metadata \n"
    response.preset = ',-map_metadata -1 -map 0 -c copy'
    response.reQueueAfter = true;
    response.processFile = true;
    response.handBrakeMode = false;
    response.FFmpegMode = true;
    response.container = '.' + file.container
    NeedsProcessed = true;
    return response

    }else{
    response.infoLog += "☑File has no title metadata"
    }
/////////////////////////////////////////////////// END DATA SECTIONS


/////////////////////////////////////////////////// BEGIN AUDIO SECTIONS
/// Function for Worker "Additional Audio Tracks"
for (var i = 0; i < file.ffProbeData.streams.length; i++) {
    var HasNonEngTrack = false
    var NeedsProcessed = false
    var audioIdx = -1
    var audioStreamsRemoved = 0
    /// Keep Track of Audio Stream Count
    try {
      if (file.ffProbeData.streams[i].codec_type.toLowerCase() == "audio") {
        audioIdx++
      }
    } catch (err) { }
/// Stream Types to Check for that arent ENG or UND
    try {
      if (file.ffProbeData.streams[i].codec_type.toLowerCase() == "audio" && !(file.ffProbeData.streams[i].tags.language.toLowerCase().includes('eng') || file.ffProbeData.streams[i].tags.language.toLowerCase().includes('und'))) {

        audioStreamsRemoved++

        if(audioStreamsRemoved == audioStreamCount){
          break;
        }

        ffmpegCommandInsert += ` -map -0:a:${audioIdx}`
        HasNonEngTrack = true
      }
    } catch (err) { }
}
  if(HasNonEngTrack) {
    var ffmpegCommand = `,-map 0 -c:v copy  -c:a copy ${ffmpegCommandInsert} -c:s copy -c:d copy`
    response.processFile = true;
    response.preset = `, -map 0 ${ffmpegCommandInsert} -c copy`
    response.container = '.' + file.container
    response.handBrakeMode = false
    response.FFmpegMode = true
    response.reQueueAfter = true;
    response.infoLog += "☒File contains tracks which are not english or undefined. Removing! \n"
    NeedsProcessed = true;
    return response

    } else {
    response.infoLog += "☑File doesn't contain tracks which are not english or undefined! \n"
    }

/// Function for Worker "Non AAC Audio Tracks"
for (var i = 0; i < file.ffProbeData.streams.length; i++) {
    var audioIdx = -1
    var ffmpegCommandInsert = ''
    var hasnonAACTrack = false
  try {
    if (file.ffProbeData.streams[i].codec_type.toLowerCase() == "audio") {
      audioIdx++
    }
  } catch (err) { }

  try {
    if (file.ffProbeData.streams[i].codec_name !== 'aac' && file.ffProbeData.streams[i].codec_type.toLowerCase() == "audio" ) {
      ffmpegCommandInsert += ` -c:a:${audioIdx} aac -vbr 4 `
      hasnonAACTrack = true

    }
  } catch (err) { }

}
  if (hasnonAACTrack == true) {

    response.processFile = true;
    response.preset = ffmpegCommand
    response.container = '.' + file.container
    response.handBrakeMode = false
    response.FFmpegMode = true
    response.reQueueAfter = true;
    response.infoLog += "☒ File has audio which is not in aac! \n"
    return response

} else {
  response.infoLog += "☑ All audio streams are in aac! \n"
}
/////////////////////////////////////////////////// END AUDIO SECTION
for (var i = 0; i < file.ffProbeData.streams.length; i++) {
  if(NeedsProcessed != true ) {
    response.processFile = false;
    response.infoLog += "☑File meets all conditions! \n"
    return response
  }
}
module.exports.details = details;
module.exports.plugin = plugin;